#! /bin/bash
# Ferran Montserrat Curs 2022-2023
# Febrer 2023
# llistar el directori
# -------------------------------
ERR_ARGS=1
ERR_NODIR=2
# validar número d'arguments
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "usage: $0 dir"
  exit $ERR_ARGS
fi
# validar si és directori
if [ ! -d $1 ] 
then
  echo "ERROR: $1 no és un directori"
  echo "usage: $0 dir"
  exit $ERR_NODIR
fi	
# xixa
dir=$1
ls $dir
exit 0
