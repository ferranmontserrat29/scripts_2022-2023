#! /bin/bash 
# Ferran Montserrat Curs 2022-2023 
# Febrer 2023
# Validar nota: suspès, aprovat, notable, excel·lent
# -------------------------------------------------- 
# 1) si num args no es correcte plegar 
if [ $# -ne 1 ] 
then   
  echo "Error numero arguments incorrecte"   
  echo "Usage: $0 nota"
exit 1
fi

# 2) validar rang nota
if [ $1 -lt 0 -o $1 -gt 10 ]
then
  echo "Error nota $1 incorrecte"
  echo "Usage: $0 nota 1-10"
exit 2
fi

# 3) xixa
if [ $1 -lt 5 ]
then
  echo "suspès"
elif [ $1 -lt 7 ]
then  
  echo "aprovat"
elif [ $1 -lt 9 ]
then  
  echo "notable"
else
  echo "excel·lent"
fi
exit 0
