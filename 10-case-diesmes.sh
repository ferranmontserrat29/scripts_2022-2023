#! /bin/bash 
# Ferran Montserrat Curs 2022-2023
# Febrer 2023 
# dir els dies que té un mes
# --------------------------------------

# validar el número d'arguments
if [ $# -ne 1 ] 
then   
  echo "Error: numero arguments incorrecte"   
  echo "Usage: $0 numero de mes"
exit 1
fi
# validar que el mes sigui entre 1-12
if [ $1 -lt 1 -o $1 -gt 12 ]
then
  echo "Error: $1 no és un mes"
  echo "Usage: rang 1-12"
exit 2
fi
# validar número de dies del mes
case $1 in
  "2")
    echo "28 dies"
    ;;
  "4"|"6"|"9"|"11")
    echo "30 dies"
    ;;
  *)
    echo "31 dies"
esac
exit 0
