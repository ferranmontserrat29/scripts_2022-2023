#! /bin/bash
# Ferran Montserrat Curs 2022-2023
# Febrer 2023
# indicar si fit és: regular, dir o link
# -----------------------------------
ERR_ARGS=1
ERR_NOEXIST=2
# validar número d'arguments
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "usage: $0 fit"
exit $ERR_ARGS
fi
# xixa
fit=$1 
if [ ! -e $fit  ]
then   
  echo "$fit no existeix"   
  exit $ERR_NOEXIST  
elif [ -f $fit ]
then   
  echo "$fit és un regular file" 
elif [ -h $fit ]
then   
  echo "$fit és un link" 
elif [ -d $fit ]
then   
  echo "$fit és un directori" 
else   
  echo "$fit és una altra cosa"	
fi 
exit 0
