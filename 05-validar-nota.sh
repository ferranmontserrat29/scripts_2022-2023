#! /bin/bash 
# Ferran Montserrat Curs 2022-2023 
# Febrer 2023
# Validar nota: suspès, aprovat 
# ------------------------------- 
# 1) si num args no es correcte plegar 
if [ $# -ne 1 ] 
then   
  echo "Error numero arguments incorrecte"   
  echo "Usage: $0 nota"
exit 1
fi

# 2) validar nota
if [ $1 -lt 0 -o $1 -gt 10 ]
then
  echo "Error nota $1 incorrecte"
  echo "Usage: $0 nota 1-10"
exit 2
fi

# 3) xixa
if [ $1 -lt 5 ]
then
  echo "suspès"
else
  echo "aprovat"
fi
exit 0
