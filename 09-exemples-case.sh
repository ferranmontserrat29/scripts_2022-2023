#! /bin/bash 
# Ferran Montserrat Curs 2022-2023
# Febrer 2023 
# exemples case 
# --------------------------------------

case $1 in
  "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
    echo "$1 és laborable"
    ;;
  "dissabte"|"diumenge")
    echo "$1 és festiu"
    ;;
  *)
    echo "$1 no és un dia"
esac
exit 0

case $1 in
  [aeiou])
    echo "$1 és una vocal"
  ;;
  [bcdfghjklmnpqrstvwxyz])
    echo "$1 és una consonant"
  ;;
  *)
    echo "$1 és una altra cosa"
esac
exit 0

case $1 in
  "pere"|"pau"|"joan")
    echo "és un nen"
  ;;
  "marta"|"anna"|"julia")
    echo "és una nena"
  ;;
  *)
    echo "no binari"
  ;;

esac
exit 0
