#! /bin/bash 
# Ferran Montserrat Ràfols ASIX-M01 
# Febrer 2023 
# Validar que té exàctament 2 args i mostrar-los
#-----------------------------------------------
## si num args no és correcte plegar 
if [ $# -ne 2 ]
then
  echo "ERROR: num args incorrecte"
  echo "$0 nom edat"
exit 1
fi

# xixa 
echo "nom: $1"
echo "edat: $2"
exit 0
