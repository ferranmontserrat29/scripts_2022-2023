#! /bin/bash 
# Ferran Montserrat Curs 2022-2023
# Febrer 2023 
# exemples for
# --------------------------------------

# 8) llistar tots els logins numerats
logins=$(cut -d: -f1 /etc/passwd | sort)
num=1
for element in $logins
do
  echo "$num: $element"
  ((num++))
done
exit 0

# 7) llistar numerats els noms dels fitxers del directori actiu
llista_noms=$(ls)
num=1
for nom in $llista_noms
do
  echo "$num: $nom"
  ((num++))
done
exit 0

# 6) llistar els arguments numerats
num=1
for arg in $*
do
  echo "$num: $arg"
  num=$((num+1))
done
exit 0

# 5) iterar per la llista d'arguments
for arg in "$@"
do
  echo "$arg"
done
exit 0

# 4) iterar per la llista d'arguments
for arg in $*
do
  echo "$arg"
done
exit 0

# 3) iterar pel valor d'una variable
llistat=$(ls)
for nom in $llistat
do
  echo "$nom"
done
exit 0 

# 2) iterar per un conjunt d'elements
for nom in "pere pau marta anna"
do
  echo "$nom"
done
exit 0

# 1) iterar per un conjunt d'elements
for nom in "pere" "pau" "marta" "anna"
do
  echo "$nom"
done
exit 0
